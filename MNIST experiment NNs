import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from keras.datasets import mnist
from keras.utils import to_categorical
from keras.layers.core import Dense, Flatten
from keras.layers import *
from keras.layers.wrappers import TimeDistributed
from keras.optimizers import Adam, SGD
import keras
from keras.models import load_model
np.random.seed(100)
import random
import collections

num_rows = 28
num_cols = 28
batch_size = 512
im_shape = (num_rows, num_cols, 1)

"""Improved digit detector - MNIST dataset

In this experiment, we will try to build the most accurate detector for the MNIST dataset.

MNIST is a dataset of 70,000 hand-written images, with a pixel size of 28 x 28. This dataset is often used for simple digit
recognition.

Five different neural networks, with different feed-forward architectures, will be trained on a dataset of 60000 images
with 28 x 28 pixel size, and then tested on a dataset of 10000 new images to gauge accuracy.  

Within the scope of this experiment, there will not be too much effort made to reduce the size of the classifiers.
In the first part of the experiment, each classifier will be kept with a size of less than two million trainable parameters, 
but few other restrictions will be imposed. The goal is to make the most reliable classifier possible.

Five models will be examined to test their accuracy. Each model will be trained on 20 epochs of a training set of 60000 images
before being tested on a set of 10000 images.

The first model is based on large CNN layers and larger layers in general. This model is referred to as the "regular model".
The second model is based on smaller, deeper CNN layers stacked on top of one another. This model uses max-pooling to reduce the size of input images, but the convolution does not reduce the image size because of padding. This model will be called the "deep model."
The third model is similar to the second, except instead of using max-pooling, the image size will be reduced by convolution. This model will be referred to as "alternative deep model".
The fourth model is based on having a large, single layer of LSTM cells at the end of all convolutional layers. This model will be called "RNN model".
The fifth model uses a layer of LSTM cells after every CNN layer. This technique is loosely inspired by a research paper on RNN image classifiers called 'Recurrent Convolutional Neural Network for Object Recognition' by Ming Liang and Xiaolin Hu (https://www.cv-foundation.org/openaccess/content_cvpr_2015/app/2B_004.pdf) This model will be called "Fully-RNN model"

Surprisingly, this little experiment suggested that the simple CNN model, containing a small number of large layers, is the most efficient single network. In the second half of the experiment,
20 small, simple CNN models were run on the same dataset, and the correct prediction was chosen by a majority vote of all 20 networks. With this method, an accuracy of 99.52% was reached on the test dataset.



"""

#Helper functions to get data in different formats (28 x 28 per image and 784 x 1 per image)

def getData():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    
    y_train = to_categorical(y_train, num_classes = 10)
    y_test = to_categorical(y_test, num_classes = 10)
    x_train = x_train.reshape(x_train.shape[0], 784, 1)
    x_test = x_test.reshape(x_test.shape[0], 784, 1)
    
    return (x_train, y_train), (x_test, y_test)

def getDataConv():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    img_rows, img_cols = 28, 28
    
    y_train = to_categorical(y_train, num_classes = 10)
    y_test = to_categorical(y_test, num_classes = 10)
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    
    return (x_train, y_train), (x_test, y_test)
    
(x_train, y_train), (x_test, y_test) = getDataConv()

Model = keras.models.Sequential()
Model.add(Conv2D(64, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
Model.add(Conv2D(64, (3, 3), padding = 'same', activation='relu'))
Model.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
Model.add(Conv2D(64, kernel_size = (3, 3), padding = 'same', activation='relu'))
Model.add(Conv2D(128, kernel_size = (3, 3), padding = 'same', activation='relu'))
Model.add(MaxPooling2D(pool_size = (2, 2), padding = 'same'))
Model.add(Conv2D(128, kernel_size = (3, 3), padding = 'same', activation='relu'))
Model.add(Conv2D(256, kernel_size = (3, 3), padding = 'same', activation='relu'))
Model.add(MaxPooling2D(pool_size = (2, 2), padding = 'same'))
Model.add(Dropout(0.50))
Model.add(Flatten())
Model.add(Dense(256, activation='relu'))
Model.add(Dropout(0.5))
Model.add(Dense(512, activation='relu'))
Model.add(Dropout(0.5))
Model.add(Dense(10, activation='softmax'))

DeepModel = keras.models.Sequential()
DeepModel.add(Conv2D(16, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
DeepModel.add(Conv2D(32, (3, 3), padding = 'same', activation='relu'))
DeepModel.add(Conv2D(32, (3, 3), padding = 'same', activation='relu'))
DeepModel.add(Conv2D(32, (3, 3), padding = 'same', activation='relu'))
DeepModel.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
DeepModel.add(Conv2D(10, kernel_size = (3, 3), padding = 'same', activation='relu'))
DeepModel.add(Conv2D(20, kernel_size = (3, 3), padding = 'same', activation='relu'))
DeepModel.add(Conv2D(22, kernel_size = (3, 3), padding = 'same', activation='relu'))
DeepModel.add(Conv2D(24, kernel_size = (3, 3), padding = 'same', activation='relu'))
DeepModel.add(MaxPooling2D(pool_size = (2, 2), padding = 'same'))
DeepModel.add(Conv2D(48, kernel_size = (3, 3), padding = 'same', activation='relu'))
DeepModel.add(Conv2D(64, kernel_size = (3, 3), padding = 'same', activation='relu'))
DeepModel.add(Flatten())
DeepModel.add(Dense(256, activation='relu'))
DeepModel.add(Dropout(0.5))
DeepModel.add(Dense(256, activation='relu'))
DeepModel.add(Dropout(0.5))
DeepModel.add(Dense(512, activation='relu'))
DeepModel.add(Dropout(0.5))
DeepModel.add(Dense(10, activation='softmax'))

AltDeepModel = keras.models.Sequential()
AltDeepModel.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(28, 28, 1)))
AltDeepModel.add(Conv2D(32, (3, 3), activation='relu'))
AltDeepModel.add(Conv2D(32, (3, 3), activation='relu'))
AltDeepModel.add(Conv2D(32, (3, 3), activation='relu'))
AltDeepModel.add(Conv2D(32, (3, 3), activation='relu'))
AltDeepModel.add(Conv2D(32, (3, 3), activation='relu'))
AltDeepModel.add(Conv2D(32, (3, 3), activation='relu'))
AltDeepModel.add(Conv2D(32, (3, 3), activation='relu'))
AltDeepModel.add(Conv2D(32, (3, 3), activation='relu'))
AltDeepModel.add(Conv2D(32, (3, 3), activation='relu'))
AltDeepModel.add(Conv2D(32, (2, 2), activation='relu'))
AltDeepModel.add(Conv2D(32, (2, 2), activation='relu'))
AltDeepModel.add(Conv2D(256, (2, 2), activation='relu'))
AltDeepModel.add(Flatten())
AltDeepModel.add(Dense(256, activation='relu'))
AltDeepModel.add(Dropout(0.5))
AltDeepModel.add(Dense(256, activation='relu'))
AltDeepModel.add(Dropout(0.5))
AltDeepModel.add(Dense(512, activation='relu'))
AltDeepModel.add(Dropout(0.5))
AltDeepModel.add(Dense(10, activation='softmax'))

RNNModel = keras.models.Sequential()
RNNModel.add(Conv2D(32, kernel_size = (3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
RNNModel.add(Conv2D(64, kernel_size = (3, 3), padding = 'same', activation='relu'))
RNNModel.add(MaxPooling2D(pool_size = (2, 2)))
RNNModel.add(Conv2D(8, kernel_size = (3, 3), padding = 'same', activation='relu'))
RNNModel.add(Dropout(0.50))
RNNModel.add(MaxPooling2D(pool_size = (2, 2)))
RNNModel.add(Conv2D(32, kernel_size = (3, 3), padding = 'same', activation='relu'))
RNNModel.add(Dropout(0.50))
RNNModel.add(MaxPooling2D(pool_size = (2, 2), padding = 'same'))
RNNModel.add(Reshape((4, 4*32)))
RNNModel.add(Dropout(0.50))
RNNModel.add(LSTM(200, return_sequences = True))
RNNModel.add(Dropout(0.50))
RNNModel.add(Dense(228, activation='relu'))
RNNModel.add(Dropout(0.5))
RNNModel.add(Flatten())
RNNModel.add(Dense(10, activation='softmax'))

FullRNNModel = keras.models.Sequential()
FullRNNModel.add(Conv2D(32, kernel_size = (3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
FullRNNModel.add(Conv2D(32, kernel_size = (3, 3), activation='relu', padding = 'same'))
FullRNNModel.add(MaxPooling2D(pool_size = (2, 2)))

FullRNNModel.add(Dropout(0.50))
FullRNNModel.add(Conv2D(16, kernel_size = (3, 3), padding = 'same', activation='relu'))
FullRNNModel.add(Reshape((14, 16*14))); FullRNNModel.add(LSTM(224, return_sequences = True)); FullRNNModel.add(Reshape((14, 14, 16)))
FullRNNModel.add(Conv2D(16, kernel_size = (3, 3), padding = 'same', activation='relu'))
FullRNNModel.add(Reshape((14, 16*14))); FullRNNModel.add(LSTM(224, return_sequences = True)); FullRNNModel.add(Reshape((14, 14, 16)))
FullRNNModel.add(MaxPooling2D(pool_size = (2, 2)))

FullRNNModel.add(Dropout(0.50))
FullRNNModel.add(Conv2D(32, kernel_size = (3, 3), padding = 'same', activation='relu'))
FullRNNModel.add(Reshape((7, 7*32))); FullRNNModel.add(LSTM(105, return_sequences = True)); FullRNNModel.add(Reshape((7, 7, 15)))
FullRNNModel.add(Conv2D(32, kernel_size = (3, 3), padding = 'same', activation='relu'))
FullRNNModel.add(Reshape((7, 7*32))); FullRNNModel.add(LSTM(105, return_sequences = True)); FullRNNModel.add(Reshape((7, 7, 15)))
FullRNNModel.add(MaxPooling2D(pool_size = (2, 2), padding = 'same'))

FullRNNModel.add(Dropout(0.50))
FullRNNModel.add(Reshape((4, 4*15))); FullRNNModel.add(LSTM(200, return_sequences = True))
FullRNNModel.add(Dropout(0.50))
FullRNNModel.add(Dense(446, activation='relu'))
FullRNNModel.add(Dropout(0.5))
FullRNNModel.add(Dense(228, activation='relu'))
FullRNNModel.add(Dropout(0.5))
FullRNNModel.add(Dense(228, activation='relu'))
FullRNNModel.add(Dropout(0.5))
FullRNNModel.add(Flatten())
FullRNNModel.add(Dense(10, activation='softmax'))

optimizer = Adam(lr = 0.001, decay = 0.000001)
Model.compile(loss=keras.losses.categorical_crossentropy, optimizer=optimizer, metrics=['accuracy'])
Model.summary()
history = Model.fit(x_train, y_train, batch_size=batch_size, epochs=20, validation_data=(x_test, y_test))
#Model.save("C:/Users/NS/Desktop/python stuff/projects/Neural Networks/model1")

optimizer = Adam(lr = 0.001, decay = 0.00001)
DeepModel.compile(loss=keras.losses.categorical_crossentropy, optimizer=optimizer, metrics=['accuracy'])
DeepModel.summary()
history = DeepModel.fit(x_train, y_train, batch_size=batch_size, epochs=20, validation_data=(x_test, y_test))
DeepModel.save("C:/Users/NS/Desktop/python stuff/projects/Neural Networks/DeepCNN")

optimizer = Adam(lr = 0.001, decay = 0.00001)
AltDeepModel.compile(loss=keras.losses.categorical_crossentropy, optimizer=optimizer, metrics=['accuracy'])
AltDeepModel.summary()
history = AltDeepModel.fit(x_train, y_train, batch_size=batch_size, epochs=20, validation_data=(x_test, y_test))
AltDeepModel.save("C:/Users/NS/Desktop/python stuff/projects/Neural Networks/VeryDeepModel")

optimizer = Adam(lr = 0.001, decay = 0.000001)
RNNModel.compile(loss=keras.losses.categorical_crossentropy, optimizer=optimizer, metrics=['accuracy'])
RNNModel.summary()
history = RNNModel.fit(x_train, y_train, batch_size=batch_size, epochs=20, validation_data=(x_test, y_test))
RNNModel.save("C:/Users/NS/Desktop/python stuff/projects/Neural Networks/RNNModel")

optimizer = Adam(lr = 0.001, decay = 0.000001)
FullRNNModel.compile(loss=keras.losses.categorical_crossentropy, optimizer=optimizer, metrics=['accuracy'])
FullRNNModel.summary()
history = FullRNNModel.fit(x_train, y_train, batch_size=batch_size, epochs=40, validation_data=(x_test, y_test))
FullRNNModel.save("C:/Users/NS/Desktop/python stuff/projects/Neural Networks/FullRNN")

score = FullRNNModel.evaluate(x_test, y_test, verbose = 0)
print("test loss: {}".format(score[0]))
print("test accuracy: {}".format(score[1]))

plt.plot(history.history['loss'])
plt.show()

#Use a model to predict classes
predicted_classes = Model.predict_classes(x_test)
predicted_classes2 = AltDeepModel.predict_classes(x_test)
predicted_classes3 = DeepModel.predict_classes(x_test)
predicted_classes4 = RNNModel.predict_classes(x_test)
predicted_classes5 = FullRNNModel.predict_classes(x_test)

final_predict_vector = []

for i in range(len(y_test)):
    final_predict = np.zeros(10)
    final_predict[predicted_classes[i]] += 1.2
    final_predict[predicted_classes2[i]] += 1
    final_predict[predicted_classes3[i]] += 1
    final_predict[predicted_classes4[i]] += 1
    final_predict[predicted_classes5[i]] += 1
    prediction = np.argmax(final_predict)
    
    final_predict_vector.append(prediction)
    

incorrect = []
for i in range(len(final_predict_vector)):
    if y_test2[i] != final_predict_vector[i]:
        incorrect.append(i)
print(incorrect); print(len(incorrect))

#Plots the incorrectly labelled pictures
j = random.choice(incorrect)
image = x_test[j, :].reshape(28, 28)
plt.imshow(image)

#This next step involves creating 20 relatively simple neural networks, all of which would train on the MNIST dataset and then "vote" for a correct answer on the test set.
#The hypothesis is - misclassifications could be the result of the network overfitting or being trained in such a way that they fail to classify only specific examples - but each
#network would overfit to misclassify different images. Having the final answer be determined by a majority vote should result in either less or no overfitting.

#The following collection of 20 neural networks tests this hypothesis, but unfortunately finds it to be incorrect.

network1 = keras.models.Sequential()
network1.add(Conv2D(64, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network1.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network1.add(Conv2D(32, kernel_size=(3, 3), padding = 'same', activation='relu'))
network1.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network1.add(Conv2D(64, kernel_size=(3, 3), padding = 'same', activation='relu'))
network1.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network1.add(Flatten())
network2.add(Dropout(0.50))
network1.add(Dense(96, activation='relu'))
network1.add(Dense(10, activation='softmax'))

network2 = keras.models.Sequential()
network2.add(Conv2D(32, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network2.add(Conv2D(48, (3, 3), padding = 'same'))
network2.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network2.add(Conv2D(18, (3, 3), padding = 'same', activation='relu'))
network2.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network2.add(Conv2D(18, (3, 3), padding = 'same'))
network2.add(Conv2D(36, (3, 3), padding = 'same'))
network2.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network2.add(Dropout(0.50))
network2.add(Flatten())
network2.add(Dense(64, activation='relu'))
network2.add(Dropout(0.50))
network2.add(Dense(96, activation='relu'))
network2.add(Dense(10, activation='softmax'))


network3 = keras.models.Sequential()
network3.add(Conv2D(48, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network3.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network3.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network3.add(Conv2D(20, (3, 3), padding = 'same', activation='relu'))
network3.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network3.add(Conv2D(18, (3, 3), padding = 'same', activation='relu'))
network3.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network3.add(Dropout(0.50))
network3.add(Flatten())
network3.add(Dense(84))
network3.add(Dropout(0.50))
network3.add(Dense(114))
network3.add(Dense(10, activation='softmax'))


network4 = keras.models.Sequential()
network4.add(Conv2D(32, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network4.add(Conv2D(48, (3, 3), padding = 'same', activation='relu'))
network4.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network4.add(Conv2D(22, (3, 3), padding = 'same', activation='relu'))
network4.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network4.add(Conv2D(22, (3, 3), padding = 'same', activation='relu'))
network4.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network4.add(Flatten())
network4.add(Dense(72, activation='relu'))
network4.add(Dropout(0.3))
network4.add(Dense(96, activation='relu'))
network4.add(Dense(10, activation='softmax'))


network5 = keras.models.Sequential()
network5.add(Conv2D(16, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network5.add(Conv2D(48, (3, 3), padding = 'same', activation='relu'))
network5.add(Conv2D(32, (3, 3), padding = 'same', activation='relu'))
network5.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network5.add(Conv2D(14, (3, 3), padding = 'same'))
network5.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network5.add(Conv2D(44, (3, 3), padding = 'same'))
network5.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network5.add(Flatten())
network5.add(Dense(132, activation='relu'))
network5.add(Dropout(0.3))
network5.add(Dense(164, activation='relu'))
network5.add(Dense(10, activation='softmax'))


network6 = keras.models.Sequential()
network6.add(Conv2D(32, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network6.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network6.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network6.add(Conv2D(16, (3, 3), padding = 'same'))
network6.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network6.add(Conv2D(86, (3, 3), padding = 'same'))
network6.add(Conv2D(64, (3, 3), padding = 'same'))
network6.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network6.add(Flatten())
network6.add(Dense(64, activation='relu'))
network6.add(Dropout(0.2))
network6.add(Dense(10, activation='softmax'))



network7 = keras.models.Sequential()
network7.add(Conv2D(32, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network7.add(Conv2D(32, (3, 3), padding = 'same'))
network7.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network7.add(Conv2D(22, (3, 3), padding = 'same'))
network7.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network7.add(Conv2D(22, (3, 3), padding = 'same'))
network7.add(Conv2D(32, (3, 3), padding = 'same'))
network7.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network7.add(Flatten())
network7.add(Dense(72, activation='relu'))
network7.add(Dropout(0.2))
network7.add(Dense(10, activation='softmax'))



network8 = keras.models.Sequential()
network8.add(Conv2D(48, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network8.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network8.add(Conv2D(22, (3, 3), padding = 'same', activation='relu'))
network8.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network8.add(Conv2D(12, (3, 3), padding = 'same'))
network8.add(Conv2D(18, (3, 3), padding = 'same', activation='relu'))
network8.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network8.add(Flatten())
network8.add(Dropout(0.50))
network8.add(Dense(84, activation='relu'))
network8.add(Dropout(0.20))
network8.add(Dense(64, activation='relu'))
network8.add(Dropout(0.2))
network8.add(Dense(10, activation='softmax'))


network9 = keras.models.Sequential()
network9.add(Conv2D(54, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network9.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network9.add(Conv2D(24, (3, 3), padding = 'same', activation='relu'))
network9.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network9.add(Conv2D(24, (3, 3), padding = 'same'))
network9.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network9.add(Flatten())
network9.add(Dropout(0.50))
network9.add(Dense(72, activation='relu'))
network9.add(Dropout(0.50))
network9.add(Dense(96, activation='relu'))
network9.add(Dense(10, activation='softmax'))


network10 = keras.models.Sequential()
network10.add(Conv2D(52, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network10.add(Conv2D(32, (3, 3), padding = 'same', activation='relu'))
network10.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network10.add(Conv2D(24, (3, 3), padding = 'same'))
network10.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network10.add(Conv2D(24, (3, 3), padding = 'same', activation='relu'))
network10.add(Conv2D(22, (3, 3), padding = 'same', activation='relu'))
network10.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network10.add(Flatten())
network10.add(Dense(32))
network10.add(Dropout(0.3))
network10.add(Dense(64, activation='relu'))
network10.add(Dense(10, activation='softmax'))


network11 = keras.models.Sequential()
network11.add(Conv2D(53, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network11.add(Conv2D(16, (4, 4), padding = 'same'))
network11.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network11.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network11.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network11.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network11.add(Conv2D(32, (3, 3), padding = 'same', activation='relu'))
network11.add(Conv2D(32, (3, 3), padding = 'same'))
network11.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network11.add(Flatten())
network11.add(Dropout(0.25))
network11.add(Dense(48, activation='relu'))
network11.add(Dropout(0.25))
network11.add(Dense(48, activation='relu'))
network11.add(Dense(10, activation='softmax'))



network12 = keras.models.Sequential()
network12.add(Conv2D(22, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network12.add(Conv2D(46, (3, 3), padding = 'same'))
network12.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network12.add(Conv2D(16, (3, 3), padding = 'same'))
network12.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network12.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network12.add(Conv2D(56, (3, 3), padding = 'same', activation='relu'))
network12.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network12.add(Flatten())
network12.add(Dense(64, activation='relu'))
network12.add(Dense(96, activation='relu'))
network12.add(Dropout(0.20))
network12.add(Dense(10, activation='softmax'))



network13 = keras.models.Sequential()
network13.add(Conv2D(24, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network13.add(Conv2D(32, (3, 3), padding = 'same', activation='relu'))
network13.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network13.add(Conv2D(32, (3, 3), padding = 'same', activation='relu'))
network13.add(Conv2D(32, (3, 3), padding = 'same', activation='relu'))
network13.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network13.add(Conv2D(48, (3, 3), padding = 'same', activation='relu'))
network13.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network13.add(Flatten())
network13.add(Dropout(0.30))
network13.add(Dense(64, activation='relu'))
network13.add(Dropout(0.30))
network13.add(Dense(96, activation='relu'))
network13.add(Dense(10, activation='softmax'))



network14 = keras.models.Sequential()
network14.add(Conv2D(25, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network14.add(Conv2D(52, (3, 3), padding = 'same', activation='relu'))
network14.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network14.add(Conv2D(16, (3, 3), padding = 'same'))
network14.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network14.add(Conv2D(48, (3, 3), padding = 'same'))
network14.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network14.add(Flatten())
network14.add(Dense(72, activation='relu'))
network14.add(Dropout(0.20))
network14.add(Dense(10, activation='softmax'))


network15 = keras.models.Sequential()
network15.add(Conv2D(26, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network15.add(Conv2D(32, (3, 3), padding = 'same', activation='relu'))
network15.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network15.add(Conv2D(16, (3, 3), padding = 'same'))
network15.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network15.add(Conv2D(22, (3, 3), padding = 'same'))
network15.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network15.add(Flatten())
network15.add(Dense(84, activation='relu'))
network15.add(Dropout(0.20))
network15.add(Dense(84, activation='relu'))
network15.add(Dense(10, activation='softmax'))



network16 = keras.models.Sequential()
network16.add(Conv2D(82, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network16.add(Conv2D(32, (3, 3), padding = 'same'))
network16.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network16.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network16.add(Conv2D(18, (3, 3), padding = 'same', activation='relu'))
network16.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network16.add(Conv2D(18, (3, 3), padding = 'same'))
network16.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network16.add(Flatten())
network16.add(Dense(84, activation='relu'))
network16.add(Dropout(0.20))
network16.add(Dense(114, activation='relu'))
network16.add(Dense(10, activation='softmax'))




network17 = keras.models.Sequential()
network17.add(Conv2D(12, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network17.add(Conv2D(48, (2, 2), padding = 'same'))
network17.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network17.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network17.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network17.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network17.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network17.add(Flatten())
network17.add(Dense(96, activation='relu'))
network17.add(Dropout(0.30))
network17.add(Dense(72))
network17.add(Dense(10, activation='softmax'))



network18 = keras.models.Sequential()
network18.add(Conv2D(48, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network18.add(Conv2D(96, (3, 3), padding = 'same', activation='relu'))
network18.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network18.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network18.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network18.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network18.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network18.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network18.add(Flatten())
network18.add(Dense(64, activation='relu'))
network18.add(Dropout(0.30))
network18.add(Dense(94, activation='relu'))
network18.add(Dense(10, activation='softmax'))




network19 = keras.models.Sequential()
network19.add(Conv2D(32, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network19.add(Conv2D(32, (3, 3), padding = 'same'))
network19.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network19.add(Conv2D(16, (3, 3), padding = 'same', activation='relu'))
network19.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network19.add(Conv2D(64, (3, 3), padding = 'same'))
network19.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network19.add(Flatten())
network19.add(Dense(72, activation='relu'))
network19.add(Dropout(0.30))
network19.add(Dense(112))
network19.add(Dense(10, activation='softmax'))



network20 = keras.models.Sequential()
network20.add(Conv2D(16, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
network20.add(Conv2D(32, (3, 3), padding = 'same'))
network20.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network20.add(Conv2D(32, (3, 3), padding = 'same', activation='relu'))
network20.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network20.add(Conv2D(48, (3, 3), padding = 'same', activation='relu'))
network20.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
network20.add(Flatten())
network20.add(Dense(64))
network20.add(Dropout(0.50))
network20.add(Dense(64))
network20.add(Dense(10, activation='softmax'))

#Function which trains and saves each model
number = 1
def train_network(model):
    optimizer = Adam(lr = 0.001, decay = 0.000001)
    model.compile(loss=keras.losses.categorical_crossentropy, optimizer=optimizer, metrics=['accuracy'])
    model.summary()
    history = model.fit(x_train, y_train, batch_size=batch_size, epochs=20, validation_data=(x_test, y_test))
    model.save("C:/Users/NS/Desktop/python stuff/projects/Neural Networks/NNV" + str(number))
    
network_list = [network1, network2, network3, network4, network5, network6, network7, network8, network9, network10, network11, network12, network13, network14, network15, network16, network17, network18, network19, network20]

for model in network_list:
    train_network(model); number += 1
    
#Use a model to predict classes
predicted_classes = np.zeros((20, 10000), dtype = 'int')
count = 0
for i in network_list:
    predictions = np.asarray(i.predict_classes(x_test))
    predicted_classes[count, :] = predictions
    count += 1
    
final_predict_vector = []

for i in range(len(y_test)):
    final_predict = []
    for a in predicted_classes:
        final_predict.append(a[i])
    
    prediction = np.argmax(np.bincount(final_predict))
    final_predict_vector.append(prediction)
    
#See which ones are categorized incorrectly
incorrect = []
for i in range(len(final_predict_vector)):
    if y_test2[i] != final_predict_vector[i]:
        incorrect.append(i)
print(incorrect); print(len(incorrect))

"""
The last part of the experiment involves creating a rough equivalent of the famous VGG-net (2014), to process MNIST data with maximum accuracy.
VGG-net was originally designed for images with size 224 x 224 x 3, while MNIST data contains 28 x 28 x 1 size images.

"""

LectureNetwork = keras.models.Sequential()
LectureNetwork.add(Conv2D(64, kernel_size=(3, 3), activation='relu', padding = 'same', input_shape=(28, 28, 1)))
LectureNetwork.add(Conv2D(64, kernel_size=(3, 3), activation='relu', padding = 'same'))
LectureNetwork.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
LectureNetwork.add(Conv2D(128, kernel_size=(3, 3), activation='relu', padding = 'same'))
LectureNetwork.add(Conv2D(128, kernel_size=(3, 3), activation='relu', padding = 'same'))
LectureNetwork.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
LectureNetwork.add(Conv2D(256, kernel_size=(3, 3), activation='relu', padding = 'same'))
LectureNetwork.add(Conv2D(256, kernel_size=(3, 3), activation='relu', padding = 'same'))
LectureNetwork.add(MaxPooling2D(pool_size=(2, 2), padding = 'same'))
LectureNetwork.add(Conv2D(512, kernel_size=(3, 3), activation='relu', padding = 'same'))
LectureNetwork.add(Conv2D(512, kernel_size=(3, 3), activation='relu', padding = 'same'))
LectureNetwork.add(Conv2D(512, kernel_size=(3, 3), activation='relu', padding = 'same'))
LectureNetwork.add(Conv2D(512, kernel_size=(3, 3), activation='relu', padding = 'same'))
LectureNetwork.add(Flatten())
LectureNetwork.add(Dense(4096, activation='relu'))
LectureNetwork.add(Dropout(0.10))
LectureNetwork.add(Dense(4096, activation='relu'))
LectureNetwork.add(Dropout(0.10))
LectureNetwork.add(Dense(1000, activation='relu'))
LectureNetwork.add(Dropout(0.10))
LectureNetwork.add(Dense(10, activation='sigmoid'))

#Stochastic gradient descent optimizer with clipping is used to avoid exploding gradients.
optimizer = SGD(lr=0.01, momentum=0.9, clipnorm=1.0)
LectureNetwork.compile(loss=keras.losses.categorical_crossentropy, optimizer=optimizer, metrics=['accuracy'])
LectureNetwork.summary()
history = LectureNetwork.fit(x_train, y_train, batch_size=batch_size, epochs=20, validation_data=(x_test, y_test))
LectureNetwork.save("C:/Users/NS/Desktop/python stuff/projects/Neural Networks/LectureNetwork")

predictions = LectureNetwork.predict_classes(x_test)

